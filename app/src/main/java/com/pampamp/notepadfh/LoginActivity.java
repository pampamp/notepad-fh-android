package com.pampamp.notepadfh;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pampamp.notepadfh.api.ApiService;
import com.pampamp.notepadfh.api.MessageExtractor;
import com.pampamp.notepadfh.utils.ApplicationConfig;
import com.pampamp.notepadfh.utils.AssetsPropertyReader;
import com.pampamp.notepadfh.utils.RetrofitClient;
import com.pampamp.notepadfh.entities.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LoginActivity";

    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(com.pampamp.notepadfh.R.layout.activity_login);
        url = new AssetsPropertyReader(this).getProperties("app.properties").getProperty("apiurl");

        final Button btnLogin = findViewById(com.pampamp.notepadfh.R.id.btnEnter);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "onClick");

                ApiService apiService = RetrofitClient.getClient(url).create(ApiService.class);
                EditText email = findViewById(com.pampamp.notepadfh.R.id.email);
                EditText password = findViewById(com.pampamp.notepadfh.R.id.password);
                String sEmail =  email.getText().toString();
                String sPassword = password.getText().toString();

                if (sEmail.matches("")) {
                    email.setError("El email no debe estar vacio");
                    return;
                }

                if (sPassword.matches("")) {
                    password.setError("El password no debe estar vacio");
                    return;
                }

                apiService.authenticateaccount(sEmail, sPassword).enqueue(
                    new Callback<User>() {
                          @Override
                          public void onResponse(Call<User> call, Response<User> response) {
                              if (response.code() == 202) {
                                  ApplicationConfig.setToken(response.body().getToken());
                                  Intent intent = new Intent(getApplicationContext(), LogEpisodeActivity.class);
                                  startActivity(intent);
                                  Log.v(TAG, "LoginActivity ok, token: " + ApplicationConfig.getToken());
                              } else if (response.code()==401) {
                                  Toast.makeText(LoginActivity.this, MessageExtractor.getMessage(response), Toast.LENGTH_LONG).show();
                              } else {
                                  Toast.makeText(LoginActivity.this, "Error intentando autenticar", Toast.LENGTH_LONG).show();
                              }
                          }

                          @Override
                          public void onFailure(Call<User> call, Throwable t) {
                              Log.v(TAG, "error");
                              Toast.makeText(LoginActivity.this, "Login error", Toast.LENGTH_SHORT).show();
                          }
                      }

                );
            }
        });

        final Button btnCreateUser = findViewById(com.pampamp.notepadfh.R.id.btnCreateUser);

        btnCreateUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), CreateUserActivity.class);
                startActivity(intent);
            }
        });
    }

}
