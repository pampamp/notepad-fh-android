package com.pampamp.notepadfh.api;

import com.google.gson.JsonElement;
import com.pampamp.notepadfh.entities.Episode;
import com.pampamp.notepadfh.entities.Treatment;
import com.pampamp.notepadfh.entities.User;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by crist on 5/19/2018.
 */


public interface ApiService {

    @POST("/api/authenticateaccount")
    @FormUrlEncoded
    Call<User> authenticateaccount(@Field("email") String email, @Field("password") String password);

    @POST("/api/createaccount")
    @FormUrlEncoded
    Call<User> createaccount(@Field("email") String email, @Field("password") String password, @Field("repassword") String repassword);


    @POST("/api/episode")
    Call<Episode> saveEpisode(@Header("token") String token, @Body Episode episode);

    @GET("/api/episode")
    Call<List<Episode>> episodeList(@Header("token") String token);


}
