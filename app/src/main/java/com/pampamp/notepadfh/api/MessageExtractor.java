package com.pampamp.notepadfh.api;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import retrofit2.Response;

/**
 * Created by crist on 5/25/2018.
 */

public class MessageExtractor {

    public static String getMessage(Response response) {
        try {
            JSONObject jObjError = new JSONObject(response.errorBody().string());
            return jObjError.getString("message");
        } catch (JSONException e) {
            return response.message();
        } catch (IOException e) {
            return response.message();
        }
    }
}
