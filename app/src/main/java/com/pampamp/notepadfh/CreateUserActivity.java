package com.pampamp.notepadfh;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.pampamp.notepadfh.api.ApiService;
import com.pampamp.notepadfh.api.MessageExtractor;
import com.pampamp.notepadfh.utils.ApplicationConfig;
import com.pampamp.notepadfh.utils.AssetsPropertyReader;
import com.pampamp.notepadfh.utils.RetrofitClient;
import com.pampamp.notepadfh.entities.User;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateUserActivity extends AppCompatActivity {

    private static final String TAG = "CreateUserActivity";
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_user);
        url = new AssetsPropertyReader(this).getProperties("app.properties").getProperty("apiurl");

        final Button btnLogin = findViewById(com.pampamp.notepadfh.R.id.btnCreateUser);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.v(TAG, "onClick");

                ApiService apiService = RetrofitClient.getClient(url).create(ApiService.class);
                EditText email = findViewById(com.pampamp.notepadfh.R.id.email);
                EditText password = findViewById(com.pampamp.notepadfh.R.id.password);
                EditText repassword = findViewById(com.pampamp.notepadfh.R.id.repassword);
                String sEmail =  email.getText().toString();
                String sPassword = password.getText().toString();
                String sRePassword = repassword.getText().toString();

                if (sEmail.matches("")) {
                    email.setError("El email no debe estar vacio");
                    return;
                }

                if (sPassword.matches("")) {
                    password.setError("El primer password no debe estar vacio");
                    return;
                }

                if (sRePassword.matches("")) {
                    repassword.setError("El segundo password no debe estar vacio");
                    return;
                }

                apiService.createaccount(sEmail, sPassword, sRePassword).enqueue(
                        new Callback<User>() {
                            @Override
                            public void onResponse(Call<User> call, Response<User> response) {
                                if (response.code() == 201) {
                                    ApplicationConfig.setToken(response.body().getToken());
                                    Intent intent = new Intent(getApplicationContext(), LogEpisodeActivity.class);
                                    startActivity(intent);
                                    Log.v(TAG, "CreateUserActivity ok, token: " + ApplicationConfig.getToken());
                                } else if (response.code()==401) {
                                    Toast.makeText(CreateUserActivity.this, MessageExtractor.getMessage(response), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(CreateUserActivity.this, "error creando usuario", Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<User> call, Throwable t) {
                                Log.v(TAG, "error");
                                Toast.makeText(CreateUserActivity.this, "error creando usuario", Toast.LENGTH_LONG).show();
                            }
                        }

                );
            }
        });
    }


}
