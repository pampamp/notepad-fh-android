package com.pampamp.notepadfh.utils;

import java.util.Properties;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by crist on 5/19/2018.
 */

public class RetrofitClient {

    private static Retrofit retrofit;

    public static Retrofit getClient(String url) {
         if (retrofit==null) {
            retrofit = new Retrofit.Builder().baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
