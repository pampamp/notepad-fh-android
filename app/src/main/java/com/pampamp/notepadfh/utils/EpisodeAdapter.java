package com.pampamp.notepadfh.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.pampamp.notepadfh.R;
import com.pampamp.notepadfh.entities.Episode;

import java.util.List;

/**
 * Created by crist on 5/19/2018.
 */

public class EpisodeAdapter extends ArrayAdapter<Episode> {

    private Context context;
    private List<Episode> episodeList;

    public EpisodeAdapter(Context context, List<Episode> episodeList) {
        super(context, R.layout.item, episodeList);
        this.context = context;
        this.episodeList = episodeList;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;

        if (row == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.item, parent, false);
        }

        Episode item = episodeList.get(position);

        TextView episodeDate = (TextView) row.findViewById(R.id.episodeDate);
        TextView episodeDescription = (TextView) row.findViewById(R.id.episodeDescription);

        episodeDescription.setText(item.getDescription());
        episodeDate.setText(item.getCreationDate());

        return row;
    }

}
