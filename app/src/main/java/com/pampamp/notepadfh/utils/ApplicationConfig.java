package com.pampamp.notepadfh.utils;

/**
 * Created by crist on 4/1/2018.
 */

public class ApplicationConfig {

    private static String token;
 
    public static String getToken() {
        return token;
    }

    public static void setToken(String token) {
        ApplicationConfig.token = token;
    }
}
