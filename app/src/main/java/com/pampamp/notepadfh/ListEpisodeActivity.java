package com.pampamp.notepadfh;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.pampamp.notepadfh.api.ApiService;
import com.pampamp.notepadfh.utils.ApplicationConfig;
import com.pampamp.notepadfh.utils.AssetsPropertyReader;
import com.pampamp.notepadfh.entities.Episode;
import com.pampamp.notepadfh.utils.EpisodeAdapter;
import com.pampamp.notepadfh.utils.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListEpisodeActivity extends MenuActivity {

    private String url;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pagination_list);
        url = new AssetsPropertyReader(this).getProperties("app.properties").getProperty("apiurl");
        ApiService apiService = RetrofitClient.getClient(url).create(ApiService.class);

        listView = (ListView) findViewById(R.id.pagination_list);

        Call<List<Episode>> episodesCallback = apiService.episodeList(ApplicationConfig.getToken());

        episodesCallback.enqueue(
                new Callback<List<Episode>>() {
                    @Override
                    public void onResponse(Call<List<Episode>> call, Response<List<Episode>> response) {
                        List<Episode> episodeList = response.body();
                        listView.setAdapter(new EpisodeAdapter(ListEpisodeActivity.this, episodeList));
                    }

                    @Override
                    public void onFailure(Call<List<Episode>> call, Throwable t) {
                        Log.v("ListEpisodeActivity", "error");
                    }
                }
        );

    }

}
