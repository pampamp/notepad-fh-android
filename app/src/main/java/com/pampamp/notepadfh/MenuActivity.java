package com.pampamp.notepadfh;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

public class MenuActivity extends AppCompatActivity {

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.layout.activity_log_episode) {
            Intent intent = new Intent(getApplicationContext(), LogEpisodeActivity.class);
            startActivity(intent);
        } else if (item.getItemId() == R.layout.pagination_list) {
            Intent intent = new Intent(getApplicationContext(), ListEpisodeActivity.class);
            startActivity(intent);
        }
        return true;
    }
}
