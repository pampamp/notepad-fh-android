package com.pampamp.notepadfh;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.pampamp.notepadfh.api.ApiService;
import com.pampamp.notepadfh.utils.ApplicationConfig;
import com.pampamp.notepadfh.utils.AssetsPropertyReader;
import com.pampamp.notepadfh.entities.Brand;
import com.pampamp.notepadfh.entities.Episode;
import com.pampamp.notepadfh.entities.Place;
import com.pampamp.notepadfh.utils.RetrofitClient;
import com.pampamp.notepadfh.entities.Treatment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LogEpisodeActivity extends MenuActivity {

    private String url;
    private static final String TAG = "LogEpisodeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_episode);
        url = new AssetsPropertyReader(this).getProperties("app.properties").getProperty("apiurl");
        Spinner dropdownBrand = findViewById(R.id.brand);
        String[] itemsBrand = new String[]{"[Marca...]", Brand.BAXTER_INMUNINE.getName(), Brand.BERINING.getName(), Brand.NOVO_NORDISK.getName()};
        ArrayAdapter<String> adapterBrand = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsBrand);
        dropdownBrand.setAdapter(adapterBrand);

        Spinner dropdownTreatment = findViewById(R.id.treatment);
        String[] itemsTreatment = new String[]{"[Tratamiento...]", Treatment.HEMORRHAGE.getName(), Treatment.PROPHYLAXIS.getName(), Treatment.CONTINUING_TREATMENT.getName()};
        ArrayAdapter<String> adapterTreatment = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsTreatment);
        dropdownTreatment.setAdapter(adapterTreatment);

        Spinner dropdownPlace = findViewById(R.id.place);
        String[] itemsPlace = new String[]{"[Lugar...]", Place.FH.getName(), Place.HOME.getName(), Place.OTHERS.getName()};
        ArrayAdapter<String> adapterPlace = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, itemsPlace);
        dropdownPlace.setAdapter(adapterPlace);

        final Button button = findViewById(com.pampamp.notepadfh.R.id.btnSave);

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText units = findViewById(com.pampamp.notepadfh.R.id.units);
                Spinner band = findViewById(R.id.brand);
                EditText lote = findViewById(R.id.lote);
                EditText description = findViewById(R.id.description);
                Spinner teatment = findViewById(R.id.treatment);
                Spinner place = findViewById(R.id.place);

                String sUnits = units.getText().toString();
                String textBrand = band.getSelectedItem().toString();
                String sLote = lote.getText().toString();
                String sDescription = description.getText().toString();
                String textTreatment = teatment.getSelectedItem().toString();
                String textPlace = place.getSelectedItem().toString();

                if (sUnits.matches("")) {
                    units.setError("Las unidades no debe estar vacias");
                    return;
                }

                if (textBrand.equals("[Marca...]")) {
                    Toast.makeText(LogEpisodeActivity.this, "Seleccione una marca", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (sLote.matches("")) {
                    lote.setError("El lote no debe estar vacio");
                    return;
                }

                if (sDescription.matches("")) {
                    description.setError("La descripcion no debe estar vacia");
                    return;
                }

                if (textBrand.equals("[Tratamiento...]")) {
                    Toast.makeText(LogEpisodeActivity.this, "Seleccione un tratamiento", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (textBrand.equals("[Lugar...]")) {
                    Toast.makeText(LogEpisodeActivity.this, "Seleccione un lugar", Toast.LENGTH_SHORT).show();
                    return;
                }

                Episode episode = new Episode();
                episode.setUnits(Integer.parseInt(sUnits));
                episode.setLote(sLote);
                episode.setBrand(Brand.get(textBrand));
                episode.setDescription(sDescription);
                episode.setTreatment(Treatment.get(textTreatment));
                episode.setPlace(Place.get(textPlace));
                ApiService apiService = RetrofitClient.getClient(url).create(ApiService.class);
                apiService.saveEpisode(ApplicationConfig.getToken(), episode).enqueue(
                    new Callback<Episode>() {
                        @Override
                        public void onResponse(Call<Episode> call, Response<Episode> response) {
                            Intent intent = new Intent(getApplicationContext(), ListEpisodeActivity.class);
                            startActivity(intent);
                        }

                        @Override
                        public void onFailure(Call<Episode> call, Throwable t) {
                            Toast.makeText(LogEpisodeActivity.this, "Error registrando episodio", Toast.LENGTH_SHORT).show();
                        }
                    }
                );
           }
        });

    }
}
