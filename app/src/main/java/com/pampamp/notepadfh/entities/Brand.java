package com.pampamp.notepadfh.entities;

public enum Brand {
    BAXTER_INMUNINE("Baxter inmunine"),
    BERINING("Berining"),
    NOVO_NORDISK("Novo Nordisk");

    private String name;

    Brand(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Brand get(String name) {
        for (Brand value : Brand.values()) {
            if (value.getName().equals(name)) {
                return value;
            }
        }
        return null;
    }


}
