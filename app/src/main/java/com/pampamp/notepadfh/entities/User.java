package com.pampamp.notepadfh.entities;


import java.util.Set;

public class User {

    private long id;
    private String firstname;
    private String lastname;
    private String phone;
    private String address;
    private String email;
    private String token;
    private Set<Episode> episodeList;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

//    public Set<Episode> getEpisodeList() {
//        return episodeList;
//    }
//
//    public void setEpisodeList(Set<Episode> episodeList) {
//        this.episodeList = episodeList;
//    }
}
