package com.pampamp.notepadfh.entities;


public enum Treatment {
    PROPHYLAXIS("Profilaxis"),
    HEMORRHAGE("Hemorragia"),
    CONTINUING_TREATMENT("Continua tratamiento");

    private Integer order;
    private String name;

    Treatment(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Treatment get(String name) {
        for (Treatment value : Treatment.values()) {
            if (value.getName().equals(name)) {
                return value;
            }
        }
        return null;
    }

}
