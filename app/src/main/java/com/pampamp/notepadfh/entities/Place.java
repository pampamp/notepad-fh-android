package com.pampamp.notepadfh.entities;

public enum Place {
    FH("Fund. Hemofilia"),
    HOME("Casa"),
    OTHERS("Otros");

    private String name;

    Place(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Place get(String name) {
        for (Place value : Place.values()) {
            if (value.getName().equals(name)) {
                return value;
            }
        }
        return null;
    }


}
